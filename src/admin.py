import sys,re,os
import requests, json, urllib.parse,time
from env import proxy,db,botAdmin,listAdmin
import pymysql
os.environ['http_proxy'] = proxy 
os.environ['HTTP_PROXY'] = proxy
os.environ['https_proxy'] = proxy
os.environ['HTTPS_PROXY'] = proxy
cur = db.cursor()

bot = botAdmin	

class admin:
	def __init__(self,chat_id,bot):
		self.chat_id = chat_id
		self.bot = bot
		print(self.chat_id)
		print(self.bot)
		
	def initialMenu(self):
		greet = "Main Menu"
		requests.get('https://api.telegram.org/bot%s/sendMessage?chat_id=%s&text=%s&parse_mode=HTML&reply_markup={"inline_keyboard":[[{"text":"List Membership","callback_data":"list membership"},{"text":"Add Membership","callback_data":"add membership"}],[{"text":"Edit Membership","callback_data":"edit membership"}]]}' %(self.bot,self.chat_id,greet))


	def registrationCheck(self):
		print(self.chat_id)
		print(listAdmin)
		if str(self.chat_id) in listAdmin :
			self.register = True
		else:
			self.register = False
			print('Non Admin detected')

	def listMembership(self):
		cur.execute("SELECT * from membership")
		listMembership = cur.fetchall()
		greet = "List of Membership\nID | Name | Max Ticket | Min Ticket | Points | Start | End | Status | 1st Prize | 2nd Prize | Num of 2nd | 3rd Prize | Num of 3rd\n"
		for y in listMembership:
			greet+=str(y[0])+' | '+str(y[1])+' | '+str(y[2])+' | '+str(y[7])+' | '+str(y[3])+' | '+str(y[4])+' | '+str(y[5])+' | '+str(y[6])+' | '+str(y[8])+' | '+str(y[9])+' | '+str(y[10])+' | '+str(y[11])+' | '+str(y[12]) +"\n"
		requests.get('https://api.telegram.org/bot%s/sendMessage?chat_id=%s&text=%s&parse_mode=HTML' %(bot,self.chat_id,greet))

	def editMembership(self):
		greet = "ID of the Membership to be edit"
		requests.get('https://api.telegram.org/bot%s/sendMessage?chat_id=%s&text=%s&parse_mode=HTML&reply_markup={"force_reply":true}' %(bot,self.chat_id,greet))
		
	def addMembership(self):
		greet = "Membership Name,Max Ticket,Min Ticket,Points,Start,End,1st Prize,2nd Prize,Num of 2nd"
		requests.get('https://api.telegram.org/bot%s/sendMessage?chat_id=%s&text=%s&parse_mode=HTML&reply_markup={"force_reply":true}' %(bot,self.chat_id,greet))