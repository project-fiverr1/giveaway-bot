import sys,re,os
import requests, json, urllib.parse,time
from env import proxy,db,bot
import pymysql,math

os.environ['http_proxy'] = proxy 
os.environ['HTTP_PROXY'] = proxy
os.environ['https_proxy'] = proxy
os.environ['HTTPS_PROXY'] = proxy
cur = db.cursor()


class menu:
	def __init__(self,chat_id,bot):
		self.chat_id = chat_id
		self.bot = bot
		print(self.chat_id)
		print(self.bot)
		
	def initialMenu(self):
		greet = "Main Menu"
		requests.get('https://api.telegram.org/bot%s/sendMessage?chat_id=%s&text=%s&parse_mode=HTML&reply_markup={"inline_keyboard":[[{"text":"Account Info","callback_data":"info"},{"text":"Membership List","callback_data":"membership list"},{"text":"Buy Membership","callback_data":"buy membership"}],[{"text":"Top Up","callback_data":"top up"},{"text":"Withdraw","callback_data":"withdraw"}]]}' %(self.bot,self.chat_id,greet))

	def withdraw(self):
		cur.execute("SELECT points from user where chat_id = '%s'" %self.chat_id)
		wallet = cur.fetchone()
		if wallet[0] < 300:
			greet = "Minimum Points to withdraw are 300"
			requests.get('https://api.telegram.org/bot%s/sendMessage?chat_id=%s&text=%s&parse_mode=HTML' %(bot,self.chat_id,greet))
		else:
			greet = "Please contact admin for withdraw"
			requests.get('https://api.telegram.org/bot%s/sendMessage?chat_id=%s&text=%s&parse_mode=HTML' %(bot,self.chat_id,greet))

	def topUp(self):
		greet = "Please contact admin for Top Up"
		requests.get('https://api.telegram.org/bot%s/sendMessage?chat_id=%s&text=%s&parse_mode=HTML' %(bot,self.chat_id,greet))


	def membershipList(self):
		cur.execute("SELECT * from membership where status != 'Hide'")
		listMembership = cur.fetchall()
		db.commit()
		greet = "List of Available Membership\nID | Name | Max Ticket | Price(Points)\n"
		for y in listMembership:
			greet+=str(y[0])+' | '+str(y[1])+' | '+str(y[2])+' | '+str(y[3])+ "\n"
		requests.get('https://api.telegram.org/bot%s/sendMessage?chat_id=%s&text=%s&parse_mode=HTML' %(bot,self.chat_id,greet))

	def buyMembership(self):
		cur.execute("SELECT * from membership where status != 'Hide'")
		listMembership = cur.fetchall()
		greet = "List of Available Membership"
		request = 'https://api.telegram.org/bot%s/sendMessage?chat_id=%s&text=%s&parse_mode=HTML&reply_markup={"inline_keyboard":[' %(bot,self.chat_id,greet)
		for y in listMembership:
			text =""+str(y[1])+' - '+str(y[3])+' points '
			request+= '[{"text":"%s","callback_data":"membershipID,%s"}],' %(text,y[0])
		request+= '[]]}'		
		print(request)
		requests.get(request)

	def subscribeMembership(self,membershipID):

		cur.execute("SELECT * from membership where id ='%s'" %membershipID)
		memberData = cur.fetchone()
		db.commit()
		cur.execute("SELECT * from user where chat_id ='%s'" %self.chat_id)
		userData = cur.fetchone()
		db.commit()
		cur.execute("SELECT id from session where status = 'active' and membership_id='%s'" %membershipID)
		sessionCheck = cur.fetchone()
		db.commit()
		if not sessionCheck:
			cur.execute("INSERT INTO session (status,membership_id) VALUES('active','%s')" %(membershipID))
			db.commit()
		cur.execute("SELECT id from session where status = 'active' and membership_id='%s'" %membershipID)
		session = cur.fetchone()
		db.commit()
		cur.execute("SELECT count(membership_id) from giveaway where giveaway_id ='%s'" %session[0])
		giveawayData = cur.fetchone()
		db.commit()
		memberMax = memberData[2]
		memberMin = memberData[7]
		userMax = math.floor(userData[5]/memberData[3])
		slotavail = memberData[5]-memberData[4]+1-giveawayData[0]
		print(slotavail)
		maxBuy = min([memberMax,slotavail])
		cur.execute("SELECT count(id) from giveaway where giveaway_id = '%s' and user_name = '%s'" %(session[0],userData[1]))
		print("SELECT count(id) from giveaway where giveaway_id = '%s' and user_name = '%s'" %(session[0],userData[1]))
		slotPurchased = cur.fetchone()
		# if slotPurchased:
		# 	maxBuy = maxBuy - slotPurchased[0]
		print('maxbuy')
		print(maxBuy)
		if slotavail < memberMin:
			minBuy = slotavail
		else:
			minBuy = memberMin
		if minBuy > maxBuy:
			minBuy = maxBuy
		print('minbuy')
		print(minBuy)
		if minBuy > userMax:
			print("Issuficient Points to buy Minimum Ticket")
			greet = "Please Top Up, you have insufficient points\nMinimum is %s and maximum is %s" %(minBuy,maxBuy)
			requests.get('https://api.telegram.org/bot%s/sendMessage?chat_id=%s&text=%s&parse_mode=HTML' %(self.bot,self.chat_id,greet))
			self.initialMenu()
		elif maxBuy == 0:
			greet = "You are already buy maximum ticket\nPlease buy another membership" 
			requests.get('https://api.telegram.org/bot%s/sendMessage?chat_id=%s&text=%s&parse_mode=HTML' %(self.bot,self.chat_id,greet))
			self.initialMenu()
		else:
			print("Balance is enough")
			greet = "How many %s|%s| membership that you need ?\nMinimum is |%s| and maximum is |%s|" %(memberData[1],memberData[0],minBuy,maxBuy)
			requests.get('https://api.telegram.org/bot%s/sendMessage?chat_id=%s&text=%s&parse_mode=HTML&reply_markup={"force_reply":true}' %(self.bot,self.chat_id,greet))
		


	def infoMenu(self):
		cur.execute("SELECT * from `user` WHERE `chat_id` = '"+str(self.chat_id)+"'")
		reg_data = cur.fetchone()
		db.commit()
		greet = "Account Info\nNick name : "+ reg_data[1]+ "\nCountry : " +reg_data[2]+ "\nBirth Date : "+ str(reg_data[3]) + "\nPoints : " + str(reg_data[5]) + "\nMembership :"
		print("Data 6 = "+ str(type(reg_data[6])))
		if reg_data[6] == 1 :
			greet+="\n- Trial Membership"
			print(greet)
		#print(greet)
		requests.get('https://api.telegram.org/bot%s/sendMessage?chat_id=%s&text=%s&parse_mode=HTML' %(self.bot,self.chat_id,greet))

	def buyTrialMembership(self):
		cur.execute("SELECT * from `user` WHERE `chat_id` = '"+str(self.chat_id)+"'")
		reg_data = cur.fetchone()
		db.commit()
		greet = "Trial Membership will costs you 2 points, your existing points : " + str(reg_data[5])
		if reg_data[5] < 2 :
			requests.get('https://api.telegram.org/bot%s/sendMessage?chat_id=%s&text=%s&parse_mode=HTML&reply_markup={"inline_keyboard":[[{"text":"Buy Point","callback_data":"buy point"},{"text":"Cancel","callback_data":"cancel trial membership"}]]}' %(self.bot,self.chat_id,greet))
		else :
			requests.get('https://api.telegram.org/bot%s/sendMessage?chat_id=%s&text=%s&parse_mode=HTML&reply_markup={"inline_keyboard":[[{"text":"Agree","callback_data":"agree trial membership"},{"text":"Cancel","callback_data":"cancel trial membership"}]]}' %(self.bot,self.chat_id,greet))
		
	def trialMembership(self):
		cur.execute("SELECT * from `user` WHERE `chat_id` = '"+str(self.chat_id)+"'")
		reg_data = cur.fetchone()
		cur.execute("UPDATE `user` set `points` = '"+str(reg_data[5] - 2)+"', `trial_membership` = True WHERE `chat_id` = '"+str(self.chat_id)+"'")
		db.commit()
		greet = "Trial Membership Purchase was Successfull"   + "\nPoints Remaining : " + str(reg_data[5]-2)
		requests.get('https://api.telegram.org/bot%s/sendMessage?chat_id=%s&text=%s&parse_mode=HTML' %(self.bot,self.chat_id,greet))

  		


	def trialMenu(self):
		cur.execute("SELECT * from `user` WHERE `chat_id` = '"+str(self.chat_id)+"'")
		reg_data = cur.fetchone()
		db.commit()
		greet = "Trial Membership Info\nNick name : "+ reg_data[1]+ "\nCountry : " +reg_data[2]+ "\nBirth Date : "+ str(reg_data[3]) + "\nPoints : " + str(reg_data[5])
		requests.get('https://api.telegram.org/bot%s/sendMessage?chat_id=%s&text=%s&parse_mode=HTML' %(self.bot,self.chat_id,greet))

	def predictionsMenu(self):
		cur.execute("SELECT * from `user` WHERE `chat_id` = '"+str(self.chat_id)+"'")
		reg_data = cur.fetchone()
		db.commit()
		greet = "Trial Hourly Predictions \nNick name : "+ reg_data[1]+ "\nCountry : " +reg_data[2]+ "\nBirth Date : "+ str(reg_data[3]) + "\nPoints : " + str(reg_data[5])
		requests.get('https://api.telegram.org/bot%s/sendMessage?chat_id=%s&text=%s&parse_mode=HTML' %(self.bot,self.chat_id,greet))

	def registrationCheck(self):
		cur.execute("SELECT points from `user` WHERE `chat_id` = '"+str(self.chat_id)+"'")
		reg_data = cur.fetchone()
		print('registrationCheck')
		print(reg_data)
		if reg_data is None:
			self.register = None
		else:
			self.register = reg_data
			print('reg_data not None')

	def nickNameCheck(self,nickname):
		if len(nickname) > 10 :
			greet = "Don't exceed more than 10 characters\nInput your nick name (max 10 characters)"
			requests.get('https://api.telegram.org/bot%s/sendMessage?chat_id=%s&text=%s&parse_mode=HTML&reply_markup={"force_reply":true}' %(self.bot,self.chat_id,greet))
		else:
			forbid = ['!', '@','#','$','%','^','&','*','(',')','+','=','[',']','{','}','<',">",',','.',"?",'"',"'",'|','~','`'] 
			res = [ele for ele in forbid if(ele in nickname)]
			print(bool(res))
			if bool(res) is True : 
				greet = "Don't use special characters\nInput your nick name (max 10 characters)"
				requests.get('https://api.telegram.org/bot%s/sendMessage?chat_id=%s&text=%s&parse_mode=HTML&reply_markup={"force_reply":true}' %(self.bot,self.chat_id,greet))
			else:
				beginCheck = re.match("[a-z,A-Z]",nickname)
				if bool(beginCheck) is False :
					greet = "Start with an alphabet\nInput your nick name (max 10 characters)"
					requests.get('https://api.telegram.org/bot%s/sendMessage?chat_id=%s&text=%s&parse_mode=HTML&reply_markup={"force_reply":true}' %(self.bot,self.chat_id,greet))
				else:
					#already taken ?
					cur.execute("SELECT * from user where nick_name = '%s'" %nickname)
					nickCheck = cur.fetchone()
					db.commit()
					if nickCheck:
						greet = "Nickname %s already taken, please input another nick name\nInput your nick name (max 10 characters)" %nickname
						requests.get('https://api.telegram.org/bot%s/sendMessage?chat_id=%s&text=%s&parse_mode=HTML&reply_markup={"force_reply":true}' %(self.bot,self.chat_id,greet))
					else:
						cur.execute("INSERT into user (`chat_id`, `nick_name`) VALUES ('"+str(self.chat_id)+"','"+str(nickname)+"')")
						db.commit()
						greet = "Input your country ID"
						requests.get('https://api.telegram.org/bot%s/sendMessage?chat_id=%s&text=%s&parse_mode=HTML&reply_markup={"force_reply":true}' %(self.bot,self.chat_id,greet))

	def countryCheck(self,country):
		print('https://api.first.org/data/v1/countries?q='+country)
		r = requests.get('https://api.first.org/data/v1/countries?q='+country)
		jsondata = r.content
		data = json.loads(jsondata)
		country = data['data']
		print(country)
		if country == []:
			print('kosong')
			greet = "Please input a country name, example : India\nInput your country ID"
			requests.get('https://api.telegram.org/bot%s/sendMessage?chat_id=%s&text=%s&parse_mode=HTML&reply_markup={"force_reply":true}' %(self.bot,self.chat_id,greet))

		else:
			print('isi')
			for x in country:
				cur.execute("UPDATE user set `country` = '"+str(country[x]['country'])+"' where chat_id = '"+str(self.chat_id)+"'")
			db.commit()
			greet = "Input your birth date (YYYY-MM-DD)"
			requests.get('https://api.telegram.org/bot%s/sendMessage?chat_id=%s&text=%s&parse_mode=HTML&reply_markup={"force_reply":true}' %(self.bot,self.chat_id,greet))

