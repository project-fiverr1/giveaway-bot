import sys

sys.path.append('/usr/local/lib/python2.7/site-packages')
import cv2
import numpy as np
import os
import pyautogui
import webbrowser
os.environ['DISPLAY'] = ':0'

webbrowser.open('http://10.37.2.188/odo.html')  # Go to example.com

output = "video.avi"
img = pyautogui.screenshot()
img = cv2.cvtColor(np.array(img), cv2.COLOR_RGB2BGR)
#get info from img
height, width, channels = img.shape
# Define the codec and create VideoWriter object
fourcc = cv2.VideoWriter_fourcc(*'mp4v')
out = cv2.VideoWriter(output, fourcc, 20.0, (width, height))

for i in range(95):
    try:
        img = pyautogui.screenshot()
        image = cv2.cvtColor(np.array(img), cv2.COLOR_RGB2BGR)
        out.write(image)
        StopIteration(0.5)
    except KeyboardInterrupt:
        break

print("finished")
out.release()
cv2.destroyAllWindows()
